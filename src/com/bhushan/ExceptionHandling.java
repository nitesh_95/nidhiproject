package com.bhushan;

import java.io.File;
import java.io.FileReader;

//Arithmetic Exception
public class ExceptionHandling {

	public static void main(String[] args) throws Exception {

		int i = 20;

		String path = "welcome.txt";
		try {
			File file = new File(path);
			FileReader fileReader = new FileReader(file);
		} catch (Exception e) {
			System.out.println("No file is present");
		}

		try {
			System.out.println(i / 0);
		} catch (Exception e) {
			System.out.println("welcome to Java");
		} finally {
			System.out.println("It always executes");
		}

	}
}
