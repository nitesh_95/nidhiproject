package com.bhushan;

class Animal {

	public void display() {
		System.out.println("I am a Animal");
	}
}

class Dog extends Animal {

	@Override
	public void display() {
		System.out.println("I am a Dog");
		super.display();
	}

}

public class SuperKeywordExample {
	public static void main(String[] args) {
		Dog dog = new Dog();
		dog.display();
	}

}
