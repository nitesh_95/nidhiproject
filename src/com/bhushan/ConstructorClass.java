package com.bhushan;


public class ConstructorClass {
	
	int id;
	int rollno;
	
	public ConstructorClass(int id, int rollno) {
		this.id = id;
		this.rollno = rollno;
	}
	
	public void display() {
		System.out.println("sum of id and roll no is " + (id + rollno));
	}
	
	public void display1() {
		System.out.println("sum of id and roll no is " + (id + rollno));
	}
	
	public static void main(String[] args) {
		ConstructorClass constructorClass = new ConstructorClass(12 ,13);
		constructorClass.display();
		constructorClass.display1();
	}

}
