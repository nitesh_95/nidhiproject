package com.bhushan;

import java.util.Scanner;

public class RectangleStarPattern {

	public static void printFilledTriangle(int width, int height) {
		for (int i = 1; i <= width; i++) {
			for (int j = 1; j <= height; j++) {
				System.out.print("* ");
			}
			System.out.print("\n");
		}
	}

	public static void printEmptyRectangle(int width, int height) {
		for (int i = 1; i <= width; i++) {
			for (int j = 1; j <= height; j++) {
				if (i == 1 || i == width || j == 1 || j == height)
					System.out.print("*");
				else
					System.out.print(" ");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		int width, height;
		Scanner scanner = new Scanner(System.in);
		System.out.print(" Please Enter Number of Width : ");
		width = scanner.nextInt();
		System.out.print(" Please Enter Number of Height : ");
		height = scanner.nextInt();
		System.out.println("The Filled Rectangle is :- ");
		printFilledTriangle(width, height);
		System.out.println("The Empty Rectangle is :- ");
		printEmptyRectangle(width, height);

	}
}
