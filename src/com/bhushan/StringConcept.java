package com.bhushan;

public class StringConcept {

	public static void main(String[] args) {

		// This is called the concept of Immutability
		String value = new String("Nitesh");
		value.concat("Bhushan"); // It goes for Garbage collection
		System.out.println("String Value is" + " " + value);

		// This is called the concept of Mutability , It is thread safe 
		StringBuilder stringBuilder = new StringBuilder("Nitesh");
		stringBuilder.append("bhushan");
		System.out.println("StringBuilder->" + " " + stringBuilder);

		// This is called the concept of Mutability , It is not thread safe 
		StringBuffer stringBuffer = new StringBuffer("Nitesh");
		stringBuffer.append("bhushan");
		System.out.println("StringBuffer->" + " " + stringBuffer);
	}

}
