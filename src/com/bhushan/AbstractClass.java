package com.bhushan;

abstract class ValuesData {
	abstract void start();
}

public class AbstractClass extends ValuesData {
	@Override
	void start() {
		System.out.println("Scooter starts with key");
	}

	public static void main(String[] args) {
		AbstractClass abstractClass = new AbstractClass();
		abstractClass.start();
	}

}
