package com.bhushan;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadFile {

	public static void main(String[] args) throws IOException, InterruptedException {
		//Read the File and get the Path ..
		FileInputStream fs = new FileInputStream("/home/niteshb/Test.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(fs));
		String lineIWant = br.readLine();
		System.out.println("linePath:" + lineIWant);
		//Run the command from the shell script through JavaCode ..
		ProcessBuilder processBuilder = new ProcessBuilder();
		String[] arguments = new String[] { lineIWant, "ls" };
		Process proc = new ProcessBuilder(arguments).start();
		int exitVal = proc.waitFor();
		if (exitVal == 0) {
			System.out.println("Success!");
			System.exit(0);
		}
	}
}