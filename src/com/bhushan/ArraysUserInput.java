package com.bhushan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ArraysUserInput {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		int length = 5;
		Scanner s = new Scanner(System.in);
		int[] numbers = new int[length];

		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		for (int i = 0; i < numbers.length; i++) {
			System.out.println("Enter" + " " + (i + 1) + " " + "number");
			numbers[i] = s.nextInt();
			arrayList.add(numbers[i]);
		}
		System.out.println("Before Reversal is " + arrayList);
		Collections.reverse(arrayList);
		System.out.println("After Reversal is " + arrayList);
	}
}