package com.bhushan.oopsConcept;


/**
 * @Example of Object OOPS concept
 *
 */
public class ObjectCreation {
	
	int number = 10;
	
	public void getNumber() {
		System.out.println("Number is "+number);
	}
	
	public static void main(String[] args) {
		ObjectCreation creation = new ObjectCreation(); //object creation of class 
		creation.getNumber(); //calling of method in ObjectCreation class 
	}

}
