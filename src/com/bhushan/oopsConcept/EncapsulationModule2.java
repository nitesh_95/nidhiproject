package com.bhushan.oopsConcept;

/**
 * @Encapsulation Concept of OOPS
 *
 *
 */
public class EncapsulationModule2 {

	public static void main(String[] args) {
		Encapsulation encapsulation = new Encapsulation();
		encapsulation.setName("bhushan");
		encapsulation.setRollno(1);
		System.out.println(encapsulation.getName() + "   " + encapsulation.getRollno());
	}

}
