package com.bhushan.oopsConcept;

/**
 * @Encapsulation Concept of OOPS
 *
 */
public class Encapsulation {
	String name;
	int rollno;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRollno() {
		return rollno;
	}

	public void setRollno(int rollno) {
		this.rollno = rollno;
	}

}
