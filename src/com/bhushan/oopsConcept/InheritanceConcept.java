package com.bhushan.oopsConcept;

/**
 * @author concept of Inheritance
 *
 */
class Employee {
	int salary = 50000;
}
public class InheritanceConcept extends Employee {
	public static void main(String[] args) {
		InheritanceConcept concept = new InheritanceConcept();
		int bonus = 10000;
		int sum = concept.salary + bonus;
		System.out.println("Total amount will be given to employee this months is " + sum);
	}
}
