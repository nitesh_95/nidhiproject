package com.bhushan.oopsConcept;

/**
 * Exmaple of class OOPS Concept ..
 *
 */
public class ClassConcept {

	String name;
	int rollno;

	public void studentStudied() {
		System.out.println("Student will try to learn ");
	}

	public static void main(String[] args) {
		ClassConcept classConcept = new ClassConcept();
		classConcept.studentStudied();
	}
}
