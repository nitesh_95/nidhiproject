package com.bhushan;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateClass {
	
	public static String dateofexpiry(String dateofdelivery, String warrantydays) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date()); 
		c.add(Calendar.DATE,Integer.parseInt(warrantydays)); 
		String output = sdf.format(c.getTime());
		System.out.println(output);
		return output;
	}
	public static void main(String[] args) {
		String dateofexpiry = dateofexpiry("16-07-2021", "100");
		System.out.println(dateofexpiry);
	}

}
