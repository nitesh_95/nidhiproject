package com.bhushan;

public class ThrowKeyword {

	public static void dividebyzero() {
		throw new ArithmeticException("Cant divide by zero");
	}

	public static void main(String[] args) {
		dividebyzero();
	}

}
