package com.bhushan;

import java.util.LinkedList;

public class LinkedLists {

	public static void main(String[] args) {
		LinkedList<Object> linkedList = new LinkedList<Object>();
		linkedList.add("name");
		linkedList.add("welcome");
		linkedList.add(0, "Nitesh");
		linkedList.add("hello");
		linkedList.addFirst("bhushan");
		System.out.println(linkedList);
		Object first = linkedList.getFirst();
		System.out.println("First element is "+first);
	}
}
