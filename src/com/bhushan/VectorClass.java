package com.bhushan;

import java.util.Vector;

public class VectorClass {

	public static  void main(String[] args) {
		Vector<String> vector = new Vector<String>();
		vector.add("welcome");
		vector.add("to");
		vector.add("Java");
		vector.add("Software");
		vector.add("classes");
		System.out.println(vector);
		
		vector.remove("software");
		System.out.println(vector);
	}
}
