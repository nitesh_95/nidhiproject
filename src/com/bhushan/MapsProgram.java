package com.bhushan;

import java.util.HashMap;

public class MapsProgram {

	public static void main(String[] args) {

		HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
		hashMap.put(1, "Response");
		hashMap.put(2, "Res");
		hashMap.put(3, "Resp");
		hashMap.put(4, "Respo");

		System.out.println(hashMap.keySet());

		if (hashMap.containsKey(10)) {
			System.out.println("It is available");
		} else {
			System.out.println("It is not available");
		}

		if (hashMap.containsValue("Respons")) {
			System.out.println("It is available");
		} else {
			System.out.println("It is not available");
		}
	}
}
