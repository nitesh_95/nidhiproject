package com.bhushan.codingskills;

import java.util.Scanner;

public class ArrayProgram {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		int sumEven = 0, sumOdd = 0;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Length?");

		int length = scanner.nextInt();
		int[] number = new int[length];
		for (int i = 0; i < number.length; i++) {
			number[i] = scanner.nextInt();
			if (number[i] <= 0) {
				break;
			} else if (number[i] % 2 == 0) {
				sumEven = sumEven + number[i];
			} else {
				sumOdd = sumOdd + number[i];
			}

		}
		System.out.println("Sum of Even number is" + " " + sumEven);
		System.out.println("Sum of Odd number is " + " " + sumOdd);
	}
}
