package com.bhushan.codingskills;

import java.util.Scanner;
//Program to print the table by Given user Input
public class PrintTable {

	public static void main(String[] args) {
	
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Number");
		int number = scanner.nextInt();
		
		for (int i = 1; i <= 10; i++) {
			int table = number * i;
			System.out.println(table);
		}
	}

}
