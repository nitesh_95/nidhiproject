package com.bhushan.codingskills;

import java.util.Scanner;

//program to check odd and even numbers
public class OddEvenProgram {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the number");
		int number = scanner.nextInt();
		
		if (number%2==0) {
			System.out.println(number +" "+" is even");
		}else {
			System.out.println(number +" "+ "is Odd");
		}
	}
}
