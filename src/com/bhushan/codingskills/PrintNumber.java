package com.bhushan.codingskills;

import java.util.Scanner;

//Program to print the number
public class PrintNumber {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the number till which you want to print");
		int number = scanner.nextInt();

		for (int i = 0; i <= number; i++) {
			System.out.println(i);
		}

	}

}
