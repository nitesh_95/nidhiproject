package com.bhushan.codingskills;

//Print even number between 1 to 100

public class PrintEvenNumber {

	public static void main(String[] args) {

		for (int i = 1; i <= 100; i++) {

			if (i % 2 == 0) {
				System.out.println(i + " ");
			}
		}

	}

}
