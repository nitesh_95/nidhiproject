package com.bhushan;

import java.util.*;

public class PalindromePrime {

	public static boolean prime(int n) {
		int c = 0;
		for (int i = 1; i <= n; i++) {
			if (n % i == 0)
				c++;
		}
		if (c == 2)
			return true;
		else
			return false;
	}

	public static boolean palindrome(int n) {
		int rev = 0, temp = n;
		while (temp != 0) {
			rev = rev * 10 + (temp % 10);
			temp = temp / 10;
		}
		if (rev == n)
			return true;
		else
			return false;
	}

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter number to be checked");
		int num = scanner.nextInt();
		if (PalindromePrime.prime(num) == true && PalindromePrime.palindrome(num) == true)
			System.out.println(num + " is  a PalPrime Number");
		else
			System.out.println(num + " is not a PalPrime Number");
	}
}
