package com.bhushan;

public class CarClass implements Vehicle {

	@Override
	public void start() {
		System.out.println("Car starts with key");
		
	}
	
	public static void main(String[] args) {
		CarClass carClass = new CarClass();
		carClass.start();
	}

}
