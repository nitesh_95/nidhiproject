package com.bhushan;

import java.util.ArrayList;

public class ArrayArrayList {
	public static void main(String[] args) {

		int i = 10;
		String valueOf = String.valueOf(i);

		String value = "10";
		int parseInt = Integer.parseInt(value);
		float parseFloat = Float.parseFloat(value);
		
		ArrayList<Float> arrayListss = new ArrayList<Float>();
		arrayListss.add(123.1f);
		arrayListss.add(2.1f);
		arrayListss.add(3.1f);
		arrayListss.add(4.1f);
		arrayListss.add(parseFloat);
		System.out.println("arraylist of float" + arrayListss);

		ArrayList<Integer> arrayLists = new ArrayList<Integer>();
		arrayLists.add(1);
		arrayLists.add(2);
		arrayLists.add(3);
		arrayLists.add(parseInt);
		System.out.println("arraylist of number" + arrayLists);

		// case of an arrayList
		// Internally size of arraylist is 10
		ArrayList<String> arrayList = new ArrayList<String>();
		arrayList.add("maruti");
		arrayList.add("fortuner");
		arrayList.add("bmw");
		arrayList.add("Audi");
		arrayList.add(valueOf);
		System.out.println(arrayList);

		arrayList.remove(3);
		System.out.println(arrayList);

		arrayList.remove("maruti");
		System.out.println(arrayList);

	}
}
