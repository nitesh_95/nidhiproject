package com.bhushan;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ThrowsKeyword {

	@SuppressWarnings("resource")
	public static String getFileName() {
		String filepath = "/home/niteshb/target/test-classes/com/planlytx/CloudGatewayApplicationTests.class";
		File file = new File(filepath);
		String name = file.getName();
		return name;
	}

	public static void getfile(String filename) throws FileNotFoundException, NullPointerException {
		File file = new File(filename);
		if (file.exists()) {
			FileInputStream fileInputStream = new FileInputStream(file);
		} else {
			throw new FileNotFoundException("If File is not available add some other file from d: directory");
		}
	}

	public static void main(String[] args) throws FileNotFoundException {
		String fileName = getFileName();
		getfile(fileName);
		System.out.println(fileName);
	}
}
