package com.bhushan;

public class AlternateNumbers {

	public static void main(String[] args) {
		int number = 20;
		for (int i = 1; i < number; i = i + 2) {
			System.out.println(i);
		}
	}
}
