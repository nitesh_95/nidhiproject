package com.bhushan;

import java.util.ArrayList;

public class Student {

	private String name;
	private int rollno;
	private int age;
	private int marks;

	public String getName() {
		return name;
	}

	public int getRollno() {
		return rollno;
	}

	public int getAge() {
		return age;
	}

	public int getMarks() {
		return marks;
	}

	public Student(String name, int rollno, int age, int marks) {
		super();
		this.name = name;
		this.rollno = rollno;
		this.age = age;
		this.marks = marks;
	}

	public static void main(String[] args) throws java.lang.Exception {

		ArrayList<Student> studentList = new ArrayList<>();
		Student code = new Student("code", 2, 26, 55);
		Student talks = new Student("talks", 1, 23, 65);
		Student dna = new Student("dna", 3, 22, 90);

		studentList.add(code);
		studentList.add(talks);
		studentList.add(dna);

		Student topStudent = null;
		int maxMark = 60;

		for (int i = 0; i < studentList.size(); i++) {
			if (studentList.get(i).getMarks() > maxMark) {
				topStudent = studentList.get(i);
				maxMark = studentList.get(i).getMarks();
				System.out.println("Top Student Details : ");
				System.out.println("Name : " + topStudent.getName());
				System.out.println("Age : " + topStudent.getAge());
				System.out.println("Roll No : " + topStudent.getRollno());
				System.out.println("Marks : " + topStudent.getMarks());
			}
		}

	}

}