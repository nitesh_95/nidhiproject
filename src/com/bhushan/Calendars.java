package com.bhushan;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Calendars {

	public static void main(String[] args) throws ParseException {
		LocalDate deliveryDate = LocalDate.parse("2021-07-10");
		LocalDate warrantyDate = deliveryDate.plusDays(180);
		LocalDate now = LocalDate.now();
		System.out.println(now);
		
		System.out.println(now.compareTo(warrantyDate));
		
		Long range = ChronoUnit.DAYS.between(now, warrantyDate);
		System.out.println("Range is "+range);
	}
}
