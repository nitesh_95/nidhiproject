package com.bhushan;

public class EqualsMethod {

	public static void main(String[] args) {

		String a = new String("Nitesh");// 16byte

		String b = new String("Nitesh");// 20byte

		// 30byte ( a , b)

		System.out.println(a == b);// false
		System.out.println(a.equals(b));// true

		String value = "BHUSHAN";
		String values = "BHUSHAN";

		System.out.println(value == values);
		
		System.out.println(value.equals(values));

	}
}
