package com.bhushan.addition;

import java.util.Scanner;

public class StarPattern2 {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter no of rows");
		int row = scanner.nextInt();
		for (int i = 1; i <= row; i++) {
			for (int j = row; j > i; j--) {
				System.out.print("");
			}
			for (int k = 1; k <= i; k++) {
				System.out.print("*");
			}
			System.out.println();
		}

	}
}
