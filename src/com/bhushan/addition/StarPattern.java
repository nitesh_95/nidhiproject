package com.bhushan.addition;

import java.util.Scanner;
//
//*
//**
//***
//****
//*****
public class StarPattern {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the number of Row");
		int row = scanner.nextInt();

		for (int i = 1; i <= row; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("*");
			}
			System.out.println();
		}

	}

}
