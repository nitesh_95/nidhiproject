package com.bhushan;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


//******************************IMP******************************
public class SortList {

	public static void main(String[] args) {
		int[] numbers = { 10, 20, 30, 80, 23, 60, 22, 50 };
		// first step is to sort the Array
		Arrays.sort(numbers);
		System.out.println(Arrays.toString(numbers));
		// Second step is to get first and last and middle element of the Array
		int num = numbers[0];
		int i = numbers[numbers.length - 1];
		int middleElement = numbers.length / 2;
		int j = numbers[middleElement];
		System.out.println("Middle element is " + j);
		System.out.println("last element of sorted array" + i);
		System.out.println("first element of sorted array" + num);
		// Third step is to get the reverse of sorted Array
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		for (Integer integer : numbers) {
			arrayList.add(integer);
		}
		Collections.reverse(arrayList);
		System.out.println("Reverse of the List is " + arrayList);
	}

}
