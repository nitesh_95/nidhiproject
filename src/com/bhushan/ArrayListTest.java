package com.bhushan;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;

public class ArrayListTest {

	@SuppressWarnings("unlikely-arg-type")
	public static void main(String[] args) {

		String path = "/home/niteshb/git/wave1/generators/src/test/com/planlytx/datascience";
		File file = new File(path);
		ArrayList<String> arrayList = new ArrayList<String>();
		File[] listFiles = file.listFiles();
		for (File file2 : listFiles) {
			String substring = file2.getName().substring(0, file2.getName().lastIndexOf('.'));
			arrayList.add(substring);
		}
		String paths = "/home/niteshb/git/wave1/generators/src/main/com/planlytx/datascience";
		File files = new File(paths);
		ArrayList<String> arrayListdata = new ArrayList<String>();
		File[] listFilesdata = files.listFiles();
		for (File file2 : listFilesdata) {
			String substring = file2.getName().substring(0, file2.getName().lastIndexOf('.'));
			arrayListdata.add(substring + "Test");
		}

		Collection<String> remainingJars = CollectionUtils.subtract(arrayListdata, arrayList);
		System.out.println(remainingJars);
	}

}
