package com.bhushan;

import java.io.File;

public class EmptyFile {

	public static void main(String[] args) {

		File folder = new File("/home/");
		File[] listOfFiles = folder.listFiles();

		for (File file : listOfFiles) {
			if (file.length() == 0) {
				System.out.println("Empty files are " + "" + file.getName());
			} else {
				System.out.println("No files are Empty");
			}
		}
	}
}
