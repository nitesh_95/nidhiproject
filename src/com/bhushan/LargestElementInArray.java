package com.bhushan;

import java.util.Arrays;

public class LargestElementInArray {
	public static void main(String[] args) {

		int[] numbers = { 21, 32, 33, 54, 35 };

		Arrays.sort(numbers);

		int largestNumber = numbers[numbers.length - 1];
		System.out.println("The largest Number is " + " " + largestNumber);
	}
}