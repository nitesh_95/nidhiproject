package com.bhushan;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String arg[]) {
		int count = 0;
		System.out.println("Enter a number ");
		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();
		for (int i = 1; i <= number; i++) {
			if (number % i == 0) {
				count++;

			}
		}
		if (count == 2)
			System.out.println("It is a prime number ");
		else
			System.out.println("It is Not a prime number ");
	}
}