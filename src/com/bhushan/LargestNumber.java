package com.bhushan;

public class LargestNumber {

	public static void main(String[] args) {

		int temp = 0;
		int[] numbers = { 21, 32, 33, 54, 35 };

		for (int i = 0; i < numbers.length; i++) {
			for (int j = i + 1; j < numbers.length; j++) {
				if (numbers[i] > numbers[j]) {
					temp = numbers[i];
					numbers[i] = numbers[j];
					numbers[j] = temp;
				}
			}
		}
		System.out.println("Largest element in the array " + numbers[numbers.length - 1]);
	}
}