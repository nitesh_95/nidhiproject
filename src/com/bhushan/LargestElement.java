package com.bhushan;

import java.util.Arrays;

public class LargestElement {

	public static void main(String[] args) {
		int[] number = { 1, 10, 12, 6, 3, 8, 15, 9 };
		Arrays.sort(number);
		int largestNumber = number[number.length-1];
		System.out.println((largestNumber));
	}
}
