package com.bhushan;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;

public class MapConcept {

	public static void main(String[] args) {

		TreeMap<String, Integer> map = new TreeMap<String, Integer>();
		map.put("Nitesh", 21);
		map.put("Nidhi", 52);
		map.put("Avinash", 60);

		System.out.println("Treemap value is " + " " + map);

		HashMap<String, Integer> maps = new HashMap<String, Integer>();
		maps.put("Nitesh", 26);
		maps.put("Nidhi", 24);
		maps.put("bhushan", 26);

		System.out.println("Hashmap value is " + " " + maps);

		LinkedHashMap<String, Integer> linkedhashmap = new LinkedHashMap<String, Integer>();
		linkedhashmap.put("Nitesh", 26);
		linkedhashmap.put("Nidhi", 24);
		linkedhashmap.put("bhushan", 26);

		System.out.println("LinkedHashmap value is " + " " + linkedhashmap);

	}
}
