package com.bhushan.area;

public class CircleArea {

	public static void main(String[] args) {
		int radius = 7;
		double area = 3.14 * radius * radius;
		System.out.println("Area of circle is " + area);
	}

}
