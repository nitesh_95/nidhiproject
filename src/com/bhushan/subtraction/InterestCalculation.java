package com.bhushan.subtraction;

import java.util.Scanner;

public class InterestCalculation {

	public static boolean getInterestValue(int Principal, float rateOfInterest, int time) {
		double interest = (Principal * rateOfInterest * time) / 100;
		System.out.println("Total interest is " + interest);
		if (interest > 10000) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter principal");
		int principal = scanner.nextInt();
		System.out.println("Enter RateOfInterest");
		float rateOfInterest = scanner.nextFloat();
		System.out.println("Enter Time");
		int time = scanner.nextInt();
		getInterestValue(principal, rateOfInterest, time);

	}
}
