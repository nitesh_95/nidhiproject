package com.bhushan.subtraction;

import java.util.Scanner;

//scanner class is used to take the input from the user at the runtime

public class Subtraction {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the first Value");
		int a = scanner.nextInt();
		
		System.out.println("Enter the Second Value");
		int b = scanner.nextInt();
		int subtract = a - b;
		System.out.println("Subtraction is " + subtract);

	}
}
