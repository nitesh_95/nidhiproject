package com.bhushan;

public class Rotation {

	public static boolean isRotation(String inputString, String expectedOutputString) {
		String stringValue = inputString + inputString;
		if (stringValue.contains(expectedOutputString)) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		String inputString = "abcd";
		String expectedOutputString = "cdab";
		boolean rotation = isRotation(inputString, expectedOutputString);
		System.out.println(rotation);
	}
}
