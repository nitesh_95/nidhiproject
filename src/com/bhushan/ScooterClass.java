package com.bhushan;

public class ScooterClass implements Vehicle {

	@Override
	public void start() {
		System.out.println("Scooter starts with kick");
	}
	
	public static void main(String[] args) {
		ScooterClass scooterClass = new ScooterClass();
		scooterClass.start();
	}
}
